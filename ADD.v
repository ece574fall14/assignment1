`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:57:49 09/01/2014 
// Design Name: 
// Module Name:    ADD 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ADD #(parameter DATA_WIDTH=8'b00001000)(a,b,sum);
	input  [DATA_WIDTH-1:0] a, b;
	output  [DATA_WIDTH-1:0] sum;
	reg sum;
	
	always@(a,b) begin
		sum<=a+b;
		end
endmodule
