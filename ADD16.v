`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:04:22 09/01/2014 
// Design Name: 
// Module Name:    ADD16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module ADD16 (a,b,sum);
	input  [`DATAWIDTH16-1:0] a, b;
	output  reg [`DATAWIDTH16-1:0] sum;

	
	always@(a,b) begin
		sum<=a+b;
		end
endmodule
