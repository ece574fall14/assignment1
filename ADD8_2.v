`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:13:57 09/04/2014 
// Design Name: 
// Module Name:    ADD8_2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module ADD8_2 (a,b,sum);
	input  [`DATAWIDTH8-1:0] a, b;
	output  reg [`DATAWIDTH8-1:0] sum;

	
	always@(a,b) begin
		sum<=a+b;
		end
endmodule
