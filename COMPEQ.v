`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:38:02 08/30/2014 
// Design Name: 
// Module Name:    COMPEQ 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module COMPEQ
#(	parameter DATA_WIDTH = 1)
(	input [DATA_WIDTH-1 : 0]a,
	input [DATA_WIDTH-1 : 0]b,
	output eq
    );
	reg eq;
	always@(*)begin
		if(a == b)
			eq = 1;
		else
			eq = 0;
	end
endmodule