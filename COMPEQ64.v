`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:18:49 09/04/2014 
// Design Name: 
// Module Name:    COMPEQ64 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module COMPEQ64(a,b,eq);

	input [`DATAWIDTH64-1 : 0]a;
	input [`DATAWIDTH64-1 : 0]b;
	output reg eq;
 
	always@(*)begin
		if(a == b)
			eq <= 1;
		else
			eq <= 0;
	end
endmodule
