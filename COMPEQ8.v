`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:53:47 09/04/2014 
// Design Name: 
// Module Name:    COMPEQ8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module COMPEQ8(a,b,eq);

	input [`DATAWIDTH8-1 : 0]a;
	input [`DATAWIDTH8-1 : 0]b;
	output reg eq;
 
	always@(*)begin
		if(a == b)
			eq <= 1;
		else
			eq <= 0;
	end
endmodule
