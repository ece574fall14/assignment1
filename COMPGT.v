`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:59:20 09/01/2014 
// Design Name: 
// Module Name:    COMPGT 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module COMPGT #(parameter DATA_WIDTH=8'b00001000)(a,b,gt);
	input  [DATA_WIDTH-1:0] a, b;
	output  gt;
	reg gt;
	
	always@(a,b) begin
		if (a>b) begin
			gt=1;
			end
		else begin
			gt=0;
		end
		end
endmodule