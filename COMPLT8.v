`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:00:50 09/01/2014 
// Design Name: 
// Module Name:    COMPLT 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module COMPLT8 #(parameter DATA_WIDTH=8'b00001000)(a,b,lt);
	input  [DATA_WIDTH-1:0] a, b;
	output reg lt;


	always@(a,b) begin
		if (a<b) begin
			lt=1;
			end
		else begin
			lt=0;
		end
		end
endmodule
