`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:05:30 09/01/2014 
// Design Name: 
// Module Name:    COMPLT_Testbench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module COMPLT_Testbench();

reg [`DATAWIDTH8-1:0] a_s8,b_s8;
reg [`DATAWIDTH16-1:0] a_s16,b_s16;
reg [`DATAWIDTH32-1:0] a_s32,b_s32;
reg [`DATAWIDTH64-1:0] a_s64,b_s64;
wire lt_s8;
wire lt_s16;
wire lt_s32;
wire lt_s64;

COMPLT8 CompToTest8(a_s8,b_s8,lt_s8);
COMPLT16 CompToTest16(a_s16,b_s16,lt_s16);
COMPLT32 CompToTest32(a_s32,b_s32,lt_s32);
COMPLT64 CompToTest64(a_s64,b_s64,lt_s64);

initial begin
a_s8<=8'b10011001;
b_s8<=8'b00010001;
a_s8<=8'b10011001;
b_s8<=8'b00010001;
a_s8<=8'b10011001;
b_s8<=8'b00010001;
a_s8<=8'b10011001;
b_s8<=8'b00010001;

#`DELAY
a_s8<=8'b11001100;
b_s8<=8'b10001000;
a_s8<=8'b11001100;
b_s8<=8'b10001000;
a_s8<=8'b11001100;
b_s8<=8'b10001000;
a_s8<=8'b11001100;
b_s8<=8'b10001000;

#`DELAY
a_s8<=8'b01000100;
b_s8<=8'b11001100;
a_s8<=8'b01000100;
b_s8<=8'b11001100;
a_s8<=8'b01000100;
b_s8<=8'b11001100;
a_s8<=8'b01000100;
b_s8<=8'b11001100;

#`DELAY
a_s8<=8'b00100010;
b_s8<=8'b01100110;
a_s8<=8'b00100010;
b_s8<=8'b01100110;
a_s8<=8'b00100010;
b_s8<=8'b01100110;
a_s8<=8'b00100010;
b_s8<=8'b01100110;

end
endmodule