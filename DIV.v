`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:09:01 08/30/2014 
// Design Name: 
// Module Name:    DIV 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
//`define DATA_WIDTH 32
module DIV
#(	parameter DATA_WIDTH = 1)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]b,
	output [DATA_WIDTH - 1 : 0]quot
    );
	reg quot;
	reg[DATA_WIDTH * 2 - 1 : 0] extendedA;
	reg[DATA_WIDTH * 2 - 1 : 0] extendedB;
	always@(a, b)begin
		extendedA = {{DATA_WIDTH{1'b0}}, a};
		extendedB = {b, {DATA_WIDTH{1'b0}}};
	end
	integer i;
	always@(extendedA, extendedB)begin
		for(i = 0; i < DATA_WIDTH; i = i + 1)begin
			extendedA = {extendedA[DATA_WIDTH * 2 - 2 : 0], 1'b0};
			if(extendedA[DATA_WIDTH * 2 - 1 : DATA_WIDTH] >= b)
				extendedA = extendedA - extendedB + 1'b1;
			else
				extendedA = extendedA;
		end
		quot = extendedA[DATA_WIDTH - 1 : 0];
	end
endmodule
