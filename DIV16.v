`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:34:44 09/05/2014 
// Design Name: 
// Module Name:    DIV16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module DIV16
//#(	parameter `DATAWIDTH16 = 1)
(	input [`DATAWIDTH16 - 1 : 0]a,
	input [`DATAWIDTH16 - 1 : 0]b,
	output [`DATAWIDTH16 - 1 : 0]quot
    );
	reg quot;
	reg[`DATAWIDTH16 * 2 - 1 : 0] extendedA;
	reg[`DATAWIDTH16 * 2 - 1 : 0] extendedB;
	always@(a, b)begin
		extendedA = {{`DATAWIDTH16{1'b0}}, a};
		extendedB = {b, {`DATAWIDTH16{1'b0}}};
	end
	integer i;
	always@(extendedA, extendedB)begin
		for(i = 0; i < `DATAWIDTH16; i = i + 1)begin
			extendedA = {extendedA[`DATAWIDTH16 * 2 - 2 : 0], 1'b0};
			if(extendedA[`DATAWIDTH16 * 2 - 1 : `DATAWIDTH16] >= b)
				extendedA = extendedA - extendedB + 1'b1;
			else
				extendedA = extendedA;
		end
		quot = extendedA[`DATAWIDTH16 - 1 : 0];
	end
endmodule