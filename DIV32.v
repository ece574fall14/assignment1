`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:37:09 09/05/2014 
// Design Name: 
// Module Name:    DIV32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module DIV32
//#(	parameter DATA_WIDTH = 1)
(	input [`DATAWIDTH32 - 1 : 0]a,
	input [`DATAWIDTH32 - 1 : 0]b,
	output [`DATAWIDTH32 - 1 : 0]quot
    );
	reg quot;
	reg[`DATAWIDTH32 * 2 - 1 : 0] extendedA;
	reg[`DATAWIDTH32 * 2 - 1 : 0] extendedB;
	always@(a, b)begin
		extendedA = {{`DATAWIDTH32{1'b0}}, a};
		extendedB = {b, {`DATAWIDTH32{1'b0}}};
	end
	integer i;
	always@(extendedA, extendedB)begin
		for(i = 0; i < `DATAWIDTH32; i = i + 1)begin
			extendedA = {extendedA[`DATAWIDTH32 * 2 - 2 : 0], 1'b0};
			if(extendedA[`DATAWIDTH32 * 2 - 1 : `DATAWIDTH32] >= b)
				extendedA = extendedA - extendedB + 1'b1;
			else
				extendedA = extendedA;
		end
		quot = extendedA[`DATAWIDTH32 - 1 : 0];
	end
endmodule