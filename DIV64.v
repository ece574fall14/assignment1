`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:40:31 09/05/2014 
// Design Name: 
// Module Name:    DIV64 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module DIV64
//#(	parameter `DATAWIDTH64 = 1)
(	input [`DATAWIDTH64 - 1 : 0]a,
	input [`DATAWIDTH64 - 1 : 0]b,
	output [`DATAWIDTH64 - 1 : 0]quot
    );
	reg quot;
	reg[`DATAWIDTH64 * 2 - 1 : 0] extendedA;
	reg[`DATAWIDTH64 * 2 - 1 : 0] extendedB;
	always@(a, b)begin
		extendedA = {{`DATAWIDTH64{1'b0}}, a};
		extendedB = {b, {`DATAWIDTH64{1'b0}}};
	end
	integer i;
	always@(extendedA, extendedB)begin
		for(i = 0; i < `DATAWIDTH64; i = i + 1)begin
			extendedA = {extendedA[`DATAWIDTH64 * 2 - 2 : 0], 1'b0};
			if(extendedA[`DATAWIDTH64 * 2 - 1 : `DATAWIDTH64] >= b)
				extendedA = extendedA - extendedB + 1'b1;
			else
				extendedA = extendedA;
		end
		quot = extendedA[`DATAWIDTH64 - 1 : 0];
	end
endmodule