`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:35:58 09/04/2014 
// Design Name: 
// Module Name:    DIV_Direct 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DIV_Direct
#(	parameter DATA_WIDTH = 8)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]b,
	output reg[DATA_WIDTH - 1 : 0]quot
    );
	always@(a, b)begin
		quot = a / b;
	end
endmodule
