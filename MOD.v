`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:44:03 08/31/2014 
// Design Name: 
// Module Name:    MOD 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MOD
#(	parameter DATA_WIDTH = 1)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]b,
	output [DATA_WIDTH - 1 : 0]rem
    );
	reg rem;
	reg[DATA_WIDTH * 2 - 1 : 0] extendedA;
	reg[DATA_WIDTH * 2 - 1 : 0] extendedB;
	always@(a, b)begin
		extendedA = {{DATA_WIDTH{1'b0}}, a};
		extendedB = {b, {DATA_WIDTH{1'b0}}};
	end
	integer i;
	always@(extendedA, extendedB)begin
		for(i = 0; i < DATA_WIDTH; i = i + 1)begin
			extendedA = {extendedA[DATA_WIDTH * 2 - 2 : 0], 1'b0};
			if(extendedA[DATA_WIDTH * 2 - 1 : DATA_WIDTH] >= b)
				extendedA = extendedA - extendedB + 1'b1;
			else
				extendedA = extendedA;
		end
		rem = extended[DATA_WIDTH * 2 - 1 : DATA_WIDTH];
	end
endmodule