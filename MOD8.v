`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:42:29 09/05/2014 
// Design Name: 
// Module Name:    MOD8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MOD8
//#(	parameter DATA_WIDTH = 1)
(	input [`DATAWIDTH8 - 1 : 0]a,
	input [`DATAWIDTH8 - 1 : 0]b,
	output [`DATAWIDTH8 - 1 : 0]rem
    );
	reg rem;
	reg[`DATAWIDTH8 * 2 - 1 : 0] extendedA;
	reg[`DATAWIDTH8 * 2 - 1 : 0] extendedB;
	always@(a, b)begin
		extendedA = {{`DATAWIDTH8{1'b0}}, a};
		extendedB = {b, {`DATAWIDTH8{1'b0}}};
	end
	integer i;
	always@(extendedA, extendedB)begin
		for(i = 0; i < `DATAWIDTH8; i = i + 1)begin
			extendedA = {extendedA[`DATAWIDTH8 * 2 - 2 : 0], 1'b0};
			if(extendedA[`DATAWIDTH8 * 2 - 1 : `DATAWIDTH8] >= b)
				extendedA = extendedA - extendedB + 1'b1;
			else
				extendedA = extendedA;
		end
		rem = extendedA[`DATAWIDTH8 * 2 - 1 : `DATAWIDTH8];
	end
endmodule