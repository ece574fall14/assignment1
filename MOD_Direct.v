`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:43:46 09/04/2014 
// Design Name: 
// Module Name:    MOD_Direct 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MOD_Direct
#(	parameter DATA_WIDTH = 8)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]b,
	output reg[DATA_WIDTH - 1 : 0]rem
    );
	always@(a, b)begin
		rem = a % b;
	end
endmodule
