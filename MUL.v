`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:01:31 09/01/2014 
// Design Name: 
// Module Name:    MUL 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MUL #(parameter DATA_WIDTH=8'b00001000)(a,b,prod);
	input  [DATA_WIDTH-1:0] a, b;
	output  [2*DATA_WIDTH-2:0] prod;
	reg prod;
	
	always@(a,b) begin
		prod<=a*b;
		end
endmodule