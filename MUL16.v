`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:11:38 09/02/2014 
// Design Name: 
// Module Name:    MUL16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUL16 (a,b,prod);
	input  [`DATAWIDTH16-1:0] a, b;
	output reg [2*(`DATAWIDTH16)-2:0] prod;

	
	always@(a,b) begin
		prod<=a*b;
		end
endmodule
