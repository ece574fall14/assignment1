`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:16:00 09/04/2014 
// Design Name: 
// Module Name:    MUL16_1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUL16_1 (a,b,prod);
	input  [`DATAWIDTH16-1:0] a, b;
	output reg [2*(`DATAWIDTH16)-2:0] prod;

	
	always@(a,b) begin
		prod<=a*b;
		end
endmodule
