`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:12:00 09/02/2014 
// Design Name: 
// Module Name:    MUL32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUL32 (a,b,prod);
	input  [`DATAWIDTH32-1:0] a, b;
	output reg [2*(`DATAWIDTH32)-2:0] prod;

	
	always@(a,b) begin
		prod<=a*b;
		end
endmodule
