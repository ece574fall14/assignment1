`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:12:21 09/02/2014 
// Design Name: 
// Module Name:    MUL64 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUL64 (a,b,prod);
	input  [`DATAWIDTH64-1:0] a, b;
	output reg [2*(`DATAWIDTH64)-2:0] prod;

	
	always@(a,b) begin
		prod<=a*b;
		end
endmodule
