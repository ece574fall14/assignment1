`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:01:31 09/01/2014 
// Design Name: 
// Module Name:    MUL 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUL8 (a,b,prod);
	input  [`DATAWIDTH8-1:0] a, b;
	output reg [2*(`DATAWIDTH8)-2:0] prod;

	
	always@(a,b) begin
		prod<=a*b;
		end
endmodule
