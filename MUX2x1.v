`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:22:03 08/30/2014 
// Design Name: 
// Module Name:    MUX2x1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MUX2x1
#(	parameter DATA_WIDTH = 1)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]b,
	input sel,
	output [DATA_WIDTH -1 : 0]d
    );
	reg d;
	always@(sel)begin
		if(sel == 0)
			d = a;
		else
			d = b;
	end
endmodule