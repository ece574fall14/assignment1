`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:40:40 09/05/2014 
// Design Name: 
// Module Name:    MUX2x164 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUX2x164 (a,b,sel,d);

	input [`DATAWIDTH64 - 1 : 0]a;
	input [`DATAWIDTH64 - 1 : 0]b;
	input sel;
	output reg [`DATAWIDTH64 - 1 : 0]d;
  
	
	always@(sel)begin
		if(sel == 0)
			d <= a;
		else
			d <= b;
	end
endmodule
