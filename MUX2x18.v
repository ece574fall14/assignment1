`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:39:51 09/04/2014 
// Design Name: 
// Module Name:    MUX2x18 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module MUX2x18 (a,b,sel,d);

	input [`DATAWIDTH8 - 1 : 0]a;
	input [`DATAWIDTH8 - 1 : 0]b;
	input sel;
	output reg [`DATAWIDTH8 - 1 : 0]d;
  
	
	always@(sel)begin
		if(sel == 0)
			d <= a;
		else
			d <= b;
	end
endmodule
