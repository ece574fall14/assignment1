`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:19:39 09/01/2014 
// Design Name: 
// Module Name:    PARAMETERS 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define	DATAWIDTH8	8
`define	DATAWIDTH16	16
`define	DATAWIDTH32	32
`define	DATAWIDTH64	64

`define  DATAWIDTH128 128
`define  DELAY			1
