`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:33:55 08/30/2014 
// Design Name: 
// Module Name:    REG 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module REG #(parameter DATA_WIDTH=8'b00001000)(d, q);
	input  [DATA_WIDTH-1:0] d;
	output  [DATA_WIDTH-1:0] q;
	reg q;
	
	always@(d) begin
		q<=d;
		end
endmodule

