`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:33:55 08/30/2014 
// Design Name: 
// Module Name:    REG 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module REG16 (d, q, Clk ,Rst);
	input Clk,Rst;
	input  [`DATAWIDTH16-1:0] d;
	output reg [`DATAWIDTH16-1:0] q;

	
	always@(posedge Clk) begin
		if (Rst==1)
			q<=16'b0;
			else
			q<=d;
		end
endmodule