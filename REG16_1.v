`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:17:50 09/04/2014 
// Design Name: 
// Module Name:    REG16_1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module REG16_1 (d, q);
	input  [`DATAWIDTH16-1:0] d;
	output reg [`DATAWIDTH16-1:0] q;

	
	always@(d) begin
		q<=d;
		end
endmodule

