`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:28:27 09/02/2014 
// Design Name: 
// Module Name:    REG32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module REG32 (d, q, Clk ,Rst);
	input Clk,Rst;
	input  [`DATAWIDTH32-1:0] d;
	output reg [`DATAWIDTH32-1:0] q;

	
	always@(posedge Clk) begin
		if (Rst==1)
			q<=32'b0;
			else
			q<=d;
		end
endmodule