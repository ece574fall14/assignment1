`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:28:44 09/02/2014 
// Design Name: 
// Module Name:    REG64 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module REG64 (d, q, Clk ,Rst);
	input Clk,Rst;
	input  [`DATAWIDTH64-1:0] d;
	output reg [`DATAWIDTH64-1:0] q;

	
	always@(posedge Clk) begin
		if (Rst==1)
			q<=64'b0;
			else
			q<=d;
		end
endmodule