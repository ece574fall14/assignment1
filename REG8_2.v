`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:19:27 09/04/2014 
// Design Name: 
// Module Name:    REG8_2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module REG8_2 (d, q);
	input  [`DATAWIDTH8-1:0] d;
	output reg [`DATAWIDTH8-1:0] q;

	
	always@(d) begin
		q<=d;
		end
endmodule

