`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:03:24 09/01/2014 
// Design Name: 
// Module Name:    REG_Testbench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module REG_Testbench();

reg [`DATAWIDTH8-1:0] d_s8;
reg [`DATAWIDTH16-1:0] d_s16;
reg [`DATAWIDTH32-1:0] d_s32;
reg [`DATAWIDTH64-1:0] d_s64;

reg Clk_s,Rst_s;

wire [`DATAWIDTH8-1:0] q_s8;
wire [`DATAWIDTH16-1:0] q_s16;
wire [`DATAWIDTH32-1:0] q_s32;
wire [`DATAWIDTH64-1:0] q_s64;



REG8 CompToTest8(d_s8, q_s8,Clk_s,Rst_s);
REG16 CompToTest16(d_s16, q_s16,Clk_s,Rst_s);
REG32 CompToTest32(d_s32, q_s32,Clk_s,Rst_s);
REG64 CompToTest64(d_s64, q_s64,Clk_s,Rst_s);

initial begin

Clk_s<=0;
Rst_s<=0;
forever #1 Clk_s=~Clk_s;



d_s8<=8'b0000_0000;
d_s16<=16'b0000_0000_0000_0000;
d_s32<=32'b0000_0000_0000_0000_0000_0000_0000_0000;
d_s64<=64'b0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000_0000;

#`DELAY
d_s8<=8'b0000_0001;
d_s16<=16'b0000_0001_0000_0001;
d_s32<=32'b0000_0001_0000_0001_0000_0001_0000_0001;
d_s64<=64'b0000_0001_0000_0001_0000_0001_0000_0001_0000_0001_0000_0001_0000_0001_0000_0001;

#`DELAY
d_s8<=8'b0000_0010;
d_s16<=16'b0000_0010_0000_0010;
d_s32<=32'b0000_0010_0000_0010_0000_0010_0000_0010;
d_s64<=64'b0000_0010_0000_0010_0000_0010_0000_0010_0000_0010_0000_0010_0000_0010_0000_0010;

#`DELAY
d_s8<=8'b0000_0100;
d_s16<=16'b0000_0100_0000_0100;
d_s32<=32'b0000_0100_0000_0100_0000_0100_0000_0100;
d_s64<=64'b0000_0100_0000_0100_0000_0100_0000_0100_0000_0100_0000_0100_0000_0100_0000_0100;

end
endmodule