`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:55:05 08/30/2014 
// Design Name: 
// Module Name:    SHL 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SHL
#(	parameter DATA_WIDTH = 1)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]sh_amt,
	output [DATA_WIDTH - 1 : 0]d
    );
	reg d;
	always@(*)begin
		d = a <<< sh_amt;
	end
endmodule