`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:23:25 09/04/2014 
// Design Name: 
// Module Name:    SHL16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module SHL16(a,sh_amt,d);

	input [`DATAWIDTH16 - 1 : 0]a;
	input [`DATAWIDTH16 - 1 : 0]sh_amt;
	output reg [`DATAWIDTH16 - 1 : 0]d;


	always@(*)begin
		d <= a <<< sh_amt;
	end
endmodule
