`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:31:26 08/30/2014 
// Design Name: 
// Module Name:    SHR 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SHR
#(	parameter DATA_WIDTH = 1)
(	input [DATA_WIDTH - 1 : 0]a,
	input [DATA_WIDTH - 1 : 0]sh_amt,
	output [DATA_WIDTH - 1 : 0]b
    );
	reg b;
	always@(*)begin
		b = a >>> sh_amt;
	end
endmodule