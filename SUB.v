`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:02:02 09/01/2014 
// Design Name: 
// Module Name:    SUB 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SUB #(parameter DATA_WIDTH=8'b00001000)(a,b,diff);
	input  [DATA_WIDTH-1:0] a, b;
	output  [DATA_WIDTH-1:0] diff;
	reg diff;
	
	always@(a,b) begin
		diff<=a-b;
		end
endmodule
