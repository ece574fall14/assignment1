`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:39:49 09/02/2014 
// Design Name: 
// Module Name:    SUB16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module SUB16 (a,b,diff);
	input  [`DATAWIDTH16-1:0] a, b;
	output reg [`DATAWIDTH16-1:0] diff;

	
	always@(a,b) begin
		diff<=a-b;
		end
endmodule
