`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:17:11 09/04/2014 
// Design Name: 
// Module Name:    SUB16_1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module SUB16_1 (a,b,diff);
	input  [`DATAWIDTH16-1:0] a, b;
	output reg [`DATAWIDTH16-1:0] diff;

	
	always@(a,b) begin
		diff<=a-b;
		end
endmodule
