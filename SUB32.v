`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:40:15 09/02/2014 
// Design Name: 
// Module Name:    SUB32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module SUB32 (a,b,diff);
	input  [`DATAWIDTH32-1:0] a, b;
	output reg [`DATAWIDTH32-1:0] diff;

	
	always@(a,b) begin
		diff<=a-b;
		end
endmodule
