`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:02:02 09/01/2014 
// Design Name: 
// Module Name:    SUB 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module SUB8 (a,b,diff);
	input  [`DATAWIDTH8-1:0] a, b;
	output reg [`DATAWIDTH8-1:0] diff;

	
	always@(a,b) begin
		diff<=a-b;
		end
endmodule
