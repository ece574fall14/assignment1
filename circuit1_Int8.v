`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:51:27 09/01/2014 
// Design Name: 
// Module Name:    circuit1_Int8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit1(a,b,c,z,x); 

input  [`DATAWIDTH8-1:0] a,b,c; 
output  [`DATAWIDTH8-1:0] z;
output  [`DATAWIDTH16-1:0] x;

wire dLTe;
wire [`DATAWIDTH8-1:0] d,e;
wire [`DATAWIDTH16-1:0] f,xwire,zwire;

	ADD8 ADD8_1(.a(a),.b(b),.sum(d));
	ADD8 ADD8_2(.a(a),.b(c),.sum(e));
	MUL16 MUL16_1(.a(a),.b(c),.prod(f));
	SUB16 SUB16_1(.a(f),.b(d),.diff(xwire));
	
	//d<=a+b; 
	
	//e<=a+c;
	
	//f<=a*c;
	
	//xwire<=f-d;
	
	//x<=xwire;
	
	REG16 REG16_1(.d(xwire),.q(x));
	COMPLT8 COMPLT8_1(.a(d),.b(e),.lt(dLTe));
	MUX2x18 MUX2x18_1(.a(d),.b(e),.sel(dLTe),.d(z));
	REG16 REG16_2(.d(zwire),.q(z));
	
	//if (d>e) begin
	//z<=d;
	//end
	//else if (d<e) begin
	//z<=e;

endmodule
