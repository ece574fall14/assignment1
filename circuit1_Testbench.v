`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:53:17 09/01/2014 
// Design Name: 
// Module Name:    circuit1_Testbench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit1_Testbench(); 
reg [`DATAWIDTH8-1:0] a_s,b_s,c_s;
wire [`DATAWIDTH8-1:0] z_s; 
wire [`DATAWIDTH16-1:0] x_s;
wire [`DATAWIDTH8-1:0] d_s,e_s;
wire [`DATAWIDTH16-1:0] f_s,xwire_S,zwire_s;
wire dLTe_s;

circuit1 CompToTest(a_s, b_s, c_s, z_s, x_s);
ADD8 ADD8_1   (a_s,b_s,sum_s);
ADD8 ADD8_2   (a_s,b_s,sum_s);
MUL16 MUL16_1	(a_s,b_s,prod_s);
SUB16 SUB16_1	(a_s,b_s,diff_s);
REG16 REG16_1	(d_s,q_s,Clk_s,Rst_s);
COMPLT8 COMPLT8_1  (a_s,b_s,lt_s);
MUX2x18 MUX2x18_1	(a_s,b_s,sel_s,d_s);
REG16 REG16_2(d_s,q_s,Clk_s,Rst_s);

initial  begin 
a_s<=8'b10001000; 
b_s<=8'b00000111;
c_s<=8'b10001001;
 
#`DELAY
a_s<=8'b10011000;
b_s<=8'b00010111;
c_s<=8'b10011001;

#`DELAY
a_s<=8'b10001001;
b_s<=8'b00001111;
c_s<=8'b10001101;
 
#`DELAY
a_s<=8'b00001010;
b_s<=8'b00000011;
c_s<=8'b00001100;
 
end
endmodule

