`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:54:33 09/01/2014 
// Design Name: 
// Module Name:    circuit2_Int32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit2(a,b,c,x,z);

input [31:0] a,b,c;
output [31:0] z,x;


wire [31:0] d,e,f,g,h;
wire dLTe, dEQe;
wire [31:0] zwire, xwire;


	//d<=a+b;
	ADD32 ADD32_1  (.a(a),.b(b),.sum(d)); 
	//e<=a+c;
	ADD32 ADD32_2 (.a(a),.b(c),.sum(e));
	//f<=a-b;
	SUB32 SUB32_1 (.a(a),.b(b),.diff(f));
	//if (d<e) begin
	//dLTe=1;
	COMPLT32 COMPLT32_1 (.a(d),.b(e),.lt(dLTe));
	//dEQe=0;
	COMPEQ32 COMPEQ32_1 (.a(d),.b(e),.eq(dEQe));
   //g<=d;
   //COMPGT32 COMPGT32_1 (.a(d),.b(e),.gt());
	//REG32 REG32_1 (.d(d),.q(g));
	MUX2x132 MUX2x132_1(.a(e),.b(d),.sel(dLTe),.d(g));
	//h<=f;
	MUX2x132 MUX2x132_2(.a(f),.b(g),.sel(dEQe),.d(h));
	
	//xwire<=(h<<1);
	SHL32 SHL32_1 (.a(h),.sh_amt(dLTe),.d(xwire));
	//zwire<=(h>>0);
	SHR32 SHR32_2 (.a(h),.sh_amt(dEQe),.d(zwire));
	
	REG32 REG32_1 (.d(xwire),.q(x));
	REG32 REG32_2 (.d(zwire),.q(z));
	//else if (d==e) begin
	//dLTe=0;
	//REG8  (.d(0),.q(dLTe));
	//dEQe<=1;
	//REG8  (.d(1),.q(dEQe));
	//g<=e;
	//REG32  (.d(e),.q(g));
	//h<=g;
	//REG  (.d(g),.q(h));
	
	//xwire<=(h<<0);
	//SHL32 SHL32_1 (.a(h),.sh_amt(0),.d(xwire));
	//REG32  (.d(h),.q(xwire));
	//zwire<=(h>>1);
	//SHR32 SHR32_1 (.a(h),.sh_amt(1),.b(zwire));
	//REG32  (.d(h),.q(zwire));

endmodule
