`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:56:11 09/01/2014 
// Design Name: 
// Module Name:    circuit2_Tesbench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit2_Tesbench();
reg [31:0] a_s,b_s,c_s;
wire [31:0] z_s,x_s;

wire [31:0] d,e,f,g,h,zwire, xwire;
wire dLTe, dEQe;

circuit2 CompToTest(a_s,b_s,c_s,z_s,x_s);
ADD32 ADD32_1  (a_s,b_s,sum_s); 
ADD32 ADD32_2 (a_s,b_s,sum_s);
SUB32 SUB32_1 (a_s,b_s,diff_s);
COMPLT32 COMPLT32_1 (a_s,b_s,lt_s);
COMPEQ32 COMPEQ32_1 (a_s,b_s,eq_s);
COMPGT32 COMPGT32_1 (a_s,b_s,gt_s);
MUX2x132 MUX2x132_1(a_s,b_s,sel_s,d_s);
MUX2x132 MUX2x132_2(a_s,b_s,sel_s,d_s);	
SHL32 SHL32_1 (a_s,sh_amt_s,d_s);
SHR32 SHR32_2 (a_s,sh_amt_s,d_s);
REG32 REG32_1 (d_s,q_s,Clk_s,Rst_s);
REG32 REG32_2 (d_s,q_s,Clk_s,Rst_s);

initial  begin
a_s<=32'b0000_1000_0000_1000_0000_1000_0000_1000;
b_s<=32'b0000_0111_0000_0111_0000_0111_0000_0111;
c_s<=32'b0000_1001_0000_1001_0000_1001_0000_1001;

#`DELAY
a_s<=32'b0001_1000_0001_1000_0001_1000_0001_1000;
b_s<=32'b0001_0111_0001_0111_0001_0111_0001_0111;
c_s<=32'b0001_1001_0001_1001_0001_1001_0001_1001;

#`DELAY
a_s<=32'b0000_1001_0000_1001_0000_1001_0000_1001;
b_s<=32'b0000_1111_0000_1111_0000_1111_0000_1111;
c_s<=32'b0000_1101_0000_1101_0000_1101_0000_1101;

#`DELAY
a_s<=32'b0000_1010_0000_1010_0000_1010_0000_1010;
b_s<=32'b0000_0011_0000_0011_0000_0011_0000_0011;
c_s<=32'b0000_1100_0000_1100_0000_1100_0000_1100;

end
endmodule
