`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:58:22 09/01/2014 
// Design Name: 
// Module Name:    circuit3_Int16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit3(a,b,c,d,e,f,g,h,sa,avg);

input [15:0] a,b,c,d,e,f,g,h;
input [7:0] sa;
output [15:0] avg;

wire [31:0] wirel00, wirel01, wirel02, wirel03, wirel10, wirel11, wirel2, wirel2div2, wirel2div4, wirel2div8;


	//l0_0 <= a + b;
	ADD32 ADD32_1 (.a({16'b0000_0000_0000_0000,a}),.b({16'b0000_0000_0000_0000,b}),.sum(wire100));
	//l0_1 <= c + d;
	ADD32 ADD32_2 (.a({16'b0000_0000_0000_0000,c}),.b({16'b0000_0000_0000_0000,d}),.sum(wire101)); 
	//l0_2 <= e + f;
	ADD32 ADD32_3(.a({16'b0000_0000_0000_0000,e}),.b({16'b0000_0000_0000_0000,f}),.sum(wire102));
	//l0_3 <= g + h;
	ADD32 ADD32_4 (.a({16'b0000_0000_0000_0000,g}),.b({16'b0000_0000_0000_0000,h}),.sum(wire103));
	//l1_0 <= l0_0 + l0_1;
	ADD32 ADD32_5 (.a(wire100),.b(wire101),.sum(wire110));
	//l1_1 <= l0_2 + l0_3;
	ADD32 ADD32_6 (.a(wire102),.b(wire103),.sum(wire111));
	//l_2 <= l1_0 + l1_1;
	ADD32 ADD32_7 (.a(wire110),.b(wire111),.sum(wire12));
	//l2div2 <= (l_2 >> sa);
	SHR32 SHR32_1 (.a(wire12),.sh_amt(sa),.d(wire12div2)); 

	//l2div4 <= (l2div2 >> sa);
	SHR32 SHR32_2 (.a(wire12div2),.sh_amt(sa),.d(wire12div4));
	
	//l2div8 <= (l2div4 >> sa);
	SHR32 SHR32_3 (.a(wire12div4),.sh_amt(sa),.d(wire12div8));
	
	//avg <= l2div8;
	REG32 REG32_1  (.d(wire12div8),.q(avg));

endmodule
