`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:01:32 09/01/2014 
// Design Name: 
// Module Name:    circuit3_Testbench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit3_Testbench();
reg [15:0] a_s,b_s,c_s,d_s,e_s,f_s,g_s,h_s;
reg [7:0] sa_s;
wire [15:0] avg_s;
wire [31:0] wirel00, wirel01, wirel02, wirel03, wirel10, wirel11, wirel2, wirel2div2, wirel2div4, wirel2div8;

circuit3 CompToTest(a_s,b_s,c_s,d_s,e_s,f_s,g_s,h_s,sa_s,avg_s);
ADD32 ADD32_1 (a_s,b_s,sum_s);
	
ADD32 ADD32_2 (a_s,b_s,sum_s);
	
ADD32 ADD32_3 (a_s,b_s,sum_s);

ADD32 ADD32_4 (a_s,b_s,sum_s);
	
ADD32 ADD32_5 (a_s,b_s,sum_s);
	
ADD32 ADD32_6 (a_s,b_s,sum_s);

ADD32 ADD32_7 (a_s,b_s,sum_s);

SHR32 SHR32_1 (a_s,sh_amt_s,d_s);

SHR32 SHR32_2 (a_s,sh_amt_s,d_s);

SHR32 SHR32_3 (a_s,sh_amt_s,d_s);

REG16 REG16_1 (d_s,q_s,Clk_s,Rst_s);

initial begin
a_s<=16'b0000_1000_0000_1000;
b_s<=16'b0000_1001_0000_1001;
c_s<=16'b0000_1100_0000_1100;
d_s<=16'b0000_1111_0000_1111;
e_s<=16'b1000_1000_1000_1000;
f_s<=16'b0100_1000_0100_1000;
g_s<=16'b0110_1000_0110_1000;
h_s<=16'b0011_1000_0011_1000;
sa_s<=8'b0000_0001;

#`DELAY
a_s<=16'b1000_1000_1000_1000;
b_s<=16'b1000_1001_1000_1001;
c_s<=16'b1000_1100_1000_1100;
d_s<=16'b1000_1111_1000_1111;
e_s<=16'b1100_1000_1100_1000;
f_s<=16'b0110_1000_0110_1000;
g_s<=16'b1110_1000_1110_1000;
h_s<=16'b1011_1000_1011_1000;
sa_s<=8'b0000_0010;

#`DELAY
a_s<=16'b0000_1001_0000_1001;
b_s<=16'b0000_1011_0000_1011;
c_s<=16'b0000_1101_0000_1101;
d_s<=16'b0010_1111_0010_1111;
e_s<=16'b1100_1001_1100_1001;
f_s<=16'b0100_1101_0100_1101;
g_s<=16'b1110_1000_1110_1000;
h_s<=16'b0011_1011_0011_1011;
sa_s<=8'b0000_0011;

#`DELAY
a_s<=16'b0100_1000_0100_1000;
b_s<=16'b0100_1001_0100_1001;
c_s<=16'b0010_1100_0010_1100;
d_s<=16'b1000_1111_1000_1111;
e_s<=16'b1010_1000_1010_1000;
f_s<=16'b0100_1010_0100_1010;
g_s<=16'b1010_1001_1010_1001;
h_s<=16'b1111_1000_1111_1000;
sa_s<=8'b0000_0100;
end
endmodule
