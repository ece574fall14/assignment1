`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:05:55 09/01/2014 
// Design Name: 
// Module Name:    circuit4_Int8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit4 (a,b,c,d,e,f,g,h,i,j,l,m,n,o,p,final);

input  [7:0] a, b, c, d, e, f, g, h, i, j, l, m, n, o, p; 
output [31:0] final;


wire [31:0] t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14;



	//t1 <= a + b;
	ADD32 ADD32_1 (.a({24'b0,a}),.b({24'b0,b}),.sum(t1));
	//t2 <= t1 + c;
	ADD32 ADD32_2 (.a(t1),.b({24'b0,c}),.sum(t2));
	//t3 <= t2 + d;
	ADD32 ADD32_3 (.a(t2),.b({24'b0,d}),.sum(t3));
	//t4 <= t3 + e;
	ADD32 ADD32_4 (.a(t3),.b({24'b0,e}),.sum(t4));
	//t5 <= t4 + f;
	ADD32 ADD32_5 (.a(t4),.b({24'b0,f}),.sum(t5));
	//t6 <= t5 + g;
	ADD32 ADD32_6 (.a(t5),.b({24'b0,g}),.sum(t6));
	//t7 <= t6 + h; 
	ADD32 ADD32_7 (.a(t6),.b({24'b0,h}),.sum(t7));
	//t8 <= t7 + i;
	ADD32 ADD32_8 (.a(t7),.b({24'b0,i}),.sum(t8));
	//t9 <= t8 + j;
	ADD32 ADD32_9 (.a(t8),.b({24'b0,j}),.sum(t9));
	//t10 <= t9 + l;
	ADD32 ADD32_10 (.a(t9),.b({24'b0,l}),.sum(t10));
	//t11 <= t10 + m; 
	ADD32 ADD32_11 (.a(10),.b({24'b0,m}),.sum(t11));
	//t12 <= t11 + n;
	ADD32 ADD32_12 (.a(t11),.b({24'b0,n}),.sum(t12));
	//t13 <= t12 + o;
	ADD32 ADD32_13 (.a(t12),.b({24'b0,o}),.sum(t13)); 
	//t14 <= t13 + p;
	ADD32 ADD32_14 (.a(t13),.b({24'b0,p}),.sum(t14));
	//final <= t14;
	REG32 REG32_1  (.d(t14),.q(final));

endmodule
