`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:10:17 09/01/2014 
// Design Name: 
// Module Name:    circuit4_Testbench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module circuit4_Testbench();
reg [7:0] a_s, b_s, c_s, d_s, e_s, f_s, g_s, h_s, i_s, j_s, l_s, m_s, n_s, o_s, p_s;
wire [31:0] final_s;
wire [31:0] t1_s, t2_s, t3_s, t4_s, t5_s, t6_s, t7_s, t8_s, t9_s, t10_s, t11_s, t12_s, t13_s, t14_s;;

	circuit4 CompToTest(a_s,b_s,c_s,d_s,e_s,f_s,g_s,h_s,i_s,j_s,l_s,m_s,n_s,o_s,p_s,final_s);
	ADD32 ADD32_1 (a_t,b_t,sum_t);
	ADD32 ADD32_2 (a_t,b_t,sum_t);
	ADD32 ADD32_3 (a_t,b_t,sum_t);
	ADD32 ADD32_4 (a_t,b_t,sum_t);
	ADD32 ADD32_5 (a_t,b_t,sum_t);
	ADD32 ADD32_6 (a_t,b_t,sum_t);
	ADD32 ADD32_7 (a_t,b_t,sum_t);
	ADD32 ADD32_8 (a_t,b_t,sum_t);
	ADD32 ADD32_9 (a_t,b_t,sum_t);
   ADD32 ADD32_10(a_t,b_t,sum_t);
	ADD32 ADD32_11(a_t,b_t,sum_t);
	ADD32 ADD32_12(a_t,b_t,sum_t); 
	ADD32 ADD32_13(a_t,b_t,sum_t);
	ADD32 ADD32_14(a_t,b_t,sum_t);
	REG32 REG32_1  (d_t,q_t,Clk_t,Rst_t);
initial begin
a_s=8'b0000_0100;
b_s=8'b0000_0101;
c_s=8'b0000_0111;
d_s=8'b0000_1000; 
e_s=8'b0001_0000;
f_s=8'b0000_0001;
g_s=8'b0010_0000;
h_s=8'b0001_1010;
i_s=8'b0001_0001;
j_s=8'b0011_1100;
l_s=8'b0000_1001;
m_s=8'b0100_0000;
n_s=8'b0000_1110;
o_s=8'b0000_0010;
p_s=8'b0000_0110;

#`DELAY
a_s=8'b0000_0100;
b_s=8'b0000_0101;
c_s=8'b0000_0111;
d_s=8'b0000_1000; 
e_s=8'b0001_0000;
f_s=8'b0000_0001;
g_s=8'b0010_0000;
h_s=8'b0001_1010;
i_s=8'b0001_0001;
j_s=8'b0011_1100;
l_s=8'b0000_1001;
m_s=8'b0100_0000;
n_s=8'b0000_1110;
o_s=8'b0000_0010;
p_s=8'b0000_0110;

#`DELAY
a_s=8'b0000_0100;
b_s=8'b0000_0101;
c_s=8'b0000_0111;
d_s=8'b0000_1000; 
e_s=8'b0001_0000;
f_s=8'b0000_0001;
g_s=8'b0010_0000;
h_s=8'b0001_1010;
i_s=8'b0001_0001;
j_s=8'b0011_1100;
l_s=8'b0000_1001;
m_s=8'b0100_0000;
n_s=8'b0000_1110;
o_s=8'b0000_0010;
p_s=8'b0000_0110;

#`DELAY
a_s=8'b0000_0100;
b_s=8'b0000_0101;
c_s=8'b0000_0111;
d_s=8'b0000_1000; 
e_s=8'b0001_0000;
f_s=8'b0000_0001;
g_s=8'b0010_0000;
h_s=8'b0001_1010;
i_s=8'b0001_0001;
j_s=8'b0011_1100;
l_s=8'b0000_1001;
m_s=8'b0100_0000;
n_s=8'b0000_1110;
o_s=8'b0000_0010;
p_s=8'b0000_0110;

end
endmodule


