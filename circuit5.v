`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:10:12 09/03/2014 
// Design Name: 
// Module Name:    circuit5 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
/*
input Int64 a, b, c

output Int32 z, x

wire Int64 d, e, f, g, h 
wire Int1 dLTe, dEQe 
reg Int64 xrin, zrin, greg, hreg

d = a + b
e = a + c
f = a - b  
dEQe = d == e
dLTe = d < e
g = dLTe ? d : e 
h = dEQe ? g : f 
greg = g
hreg = h
xrin = hreg << dLTe
zrin = hreg >> dEQe
x = xrin
z = zrin*/
`include "PARAMETERS.v"
module circuit5(
	input [`DATAWIDTH64 - 1 : 0]a,
	input [`DATAWIDTH64 - 1 : 0]b,
	input [`DATAWIDTH64 - 1 : 0]c,
	output [`DATAWIDTH32 - 1 : 0]z,
	output [`DATAWIDTH32 - 1 : 0]x
    );
	wire[`DATAWIDTH64 - 1 : 0]d, e, f, g, h;
	wire dLTe, dEQe;
	wire[`DATAWIDTH64 - 1 : 0] xrin, zrin, greg, hreg;
	//reg[`DATAWIDTH64 - 1 : 0] xrin, zrin, greg, hreg;// reg or wire?
	//d = a + b
	ADD64 adder1(.a(a), .b(b), .sum(d));
	//e = a + c
	ADD64 adder2(.a(a), .b(c), .sum(e));
	//f = a - b 
	SUB64 sub1(.a(a), .b(b), .diff(f));
	//dEQe = d == e
	COMPEQ64 compeq1(.a(d), .b(e), .eq(dEQe));
	//dLTe = d < e
	COMPLT64 complt1(.a(d), .b(e), .lt(dLTe));
	//g = dLTe ? d : e 
	MUX2x164 mux1(.a(d), .b(e), .sel(dLTe), .d(g));
	//h = dEQe ? g : f 
	MUX2x164 mux2(.a(g), .b(f), .sel(dEQe), .d(h));
	//greg = g
	REG64 reg1(.d(g), .q(greg));
	//hreg = h
	REG64 reg2(.d(h), .q(hreg));
	//xrin = hreg << dLTe
	SHL64 shl1(.a(hreg), .sh_amt({63'b0, dLTe}), .d(xrin));
	//zrin = greg >> dEQe
	SHR64 shr1(.a(greg), .sh_amt({63'b0, dEQe}), .d(zrin));
	//x = xrin
	REG32 reg3(.d(xrin), .q(x));
	//z = zrin
	REG32 reg4(.d(zrin), .q(z));
endmodule
