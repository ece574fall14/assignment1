`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:02:01 09/03/2014 
// Design Name: 
// Module Name:    circuit6 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
/*
input Int16 a, b, c, d, e, f, g, h, num

output Int16 avg

reg Int16 r1, r2, r3, r4, r5, r6, r7 

wire Int16 avgwire
wire Int32 t1, t2, t3, t4, t5, t6, t7

t1 = a + b
r1 = t1
t2 = r1 + c 
r2 = t2
t3 = r2 + d 
r3 = t3
t4 = r3 + e 
r4 = t4
t5 = r4 + f 
r5 = t5
t6 = r5 + g 
r6 = t6
t7 = r6 + h 
r7 = t7
avgwire = r7 / num
avg = avgwire*/
`include "PARAMETERS.v"
module circuit6(
	input [`DATAWIDTH16 - 1 : 0] a,
	input [`DATAWIDTH16 - 1 : 0] b,
	input [`DATAWIDTH16 - 1 : 0] c,
	input [`DATAWIDTH16 - 1 : 0] d,
	input [`DATAWIDTH16 - 1 : 0] e,
	input [`DATAWIDTH16 - 1 : 0] f,
	input [`DATAWIDTH16 - 1 : 0] g,
	input [`DATAWIDTH16 - 1 : 0] h,
	input [`DATAWIDTH16 - 1 : 0] num,
	output [`DATAWIDTH16 - 1 : 0] avg
    );
	wire[`DATAWIDTH16 - 1 : 0] r1, r2, r3, r4, r5, r6, r7;
	wire[`DATAWIDTH16 - 1 : 0] avgwire;
	wire[`DATAWIDTH32 - 1 : 0] t1, t2, t3, t4, t5, t6, t7;
	//t1 = a + b
	ADD32 adder1(.a(a), .b(b), .sum(t1));
	//r1 = t1
	REG32 reg1(.d(t1), .q(ri));
	//t2 = r1 + c
	ADD32 adder2(.a(r1), .b(c), .sum(t2));
	//r2 = t2
	REG32 reg2(.d(t2), .q(r2));
	//t3 = r2 + d 
	ADD32 adder3(.a(r2), .b(d), .sum(t3));
	//r3 = t3
	REG32 reg3(.d(t3), .q(r3));
	//t4 = r3 + e 
	ADD32 adder4(.a(r3), .b(e), .sum(t4));
	//r4 = t4
	REG32 reg4(.d(t4), .q(r4));
	//t5 = r4 + f 
	ADD32 adder5(.a(r4), .b(f), .sum(t5));
	//r5 = t5
	REG32 reg5(.d(t5), .q(r5));
	//t6 = r5 + g 
	ADD32 adder6(.a(r5), .b(g), .sum(t6));
	//r6 = t6
	REG32 reg6(.d(t6), .q(r6));
	//t7 = r6 + h 
	ADD32 adder7(.a(r6), .b(h), .sum(t7));
	//r7 = t7
	REG32 reg7(.d(t7), .q(r7));
	//avgwire = r7 / num
	DIV32 div1(.a(r7), .b(num), .quot(avgwire));
	//avg = avgwire
	REG32 reg8(.d(avgwire), .q(avg));
endmodule
