`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:06:47 09/03/2014 
// Design Name: 
// Module Name:    circuit7 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
/*
input Int64 a, b, c, d, zero

output Int64 z

wire Int64 e, f, g, zwire
wire Int1 gEQz  

e = a / b
f = c / d
g = a % b  
gEQz = g == zero
zwire = gEQz ? e : f 
z = zwire*/
`include "PARAMETERS.v"
module circuit7(
	input[`DATAWIDTH64 - 1 : 0] a,
	input[`DATAWIDTH64 - 1 : 0] b,
	input[`DATAWIDTH64 - 1 : 0] c,
	input[`DATAWIDTH64 - 1 : 0] d,
	input[`DATAWIDTH64 - 1 : 0] zero,
	output[`DATAWIDTH64 - 1 : 0] z
    );
	wire[`DATAWIDTH64 - 1 : 0] e, f, g, zwire;
	wire gEQz;
	//e = a / b
	DIV64 div1(.a(a), .b(b), .quot(e));
	//f = c / d
	DIV64 div2(.a(c), .b(d), .quot(f));
	//g = a % b
	MOD64 mod1(.a(a), .b(b), .rem(g));
	//gEQz = g == zero
	COMPEQ64 compeq1(.a(g), .b(zero), .eq(gEQz));
	//zwire = gEQz ? e : f 
	MUX2x164 mux1(.a(e), .b(f), .sel(gEQz), .d(zwire));
	//z = zwire
	REG64 reg1(.d(zwire), .q(z));
endmodule
