/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000000980722406_1733214092_init();
    work_m_00000000000980722406_3346106068_init();
    work_m_00000000000980722406_4062847055_init();
    work_m_00000000000980722406_1715368255_init();
    work_m_00000000002031006588_1621113995_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000002031006588_1621113995");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
