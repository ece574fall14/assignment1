/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000000980722406_1733214092_init();
    work_m_00000000002695029990_3828812439_init();
    work_m_00000000001634991594_0830017150_init();
    work_m_00000000001403390394_1523299002_init();
    work_m_00000000004142467248_2859544291_init();
    work_m_00000000003666989343_1522827671_init();
    work_m_00000000001769481730_0180160581_init();
    work_m_00000000002209251147_0076388714_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000002209251147_0076388714");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
