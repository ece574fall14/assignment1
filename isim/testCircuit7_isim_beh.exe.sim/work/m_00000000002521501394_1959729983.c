/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "E:/workspace/ECE574Assignment1/MOD64.v";
static unsigned int ng1[] = {0U, 0U, 0U, 0U};
static int ng2[] = {0, 0};
static int ng3[] = {64, 0};
static unsigned int ng4[] = {0U, 0U};
static unsigned int ng5[] = {1U, 0U, 0U, 0U, 0U, 0U, 0U, 0U};
static int ng6[] = {1, 0};



static void Always_31_0(char *t0)
{
    char t4[32];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;

LAB0:    t1 = (t0 + 3008U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(31, ng0);
    t2 = (t0 + 3576);
    *((int *)t2) = 1;
    t3 = (t0 + 3040);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(31, ng0);

LAB5:    xsi_set_current_line(32, ng0);
    t5 = (t0 + 1048U);
    t6 = *((char **)t5);
    t5 = ((char*)((ng1)));
    xsi_vlogtype_concat(t4, 128, 128, 2U, t5, 64, t6, 64);
    t7 = (t0 + 1768);
    xsi_vlogvar_assign_value(t7, t4, 0, 0, 128);
    xsi_set_current_line(33, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 1208U);
    t5 = *((char **)t3);
    xsi_vlogtype_concat(t4, 128, 128, 2U, t5, 64, t2, 64);
    t3 = (t0 + 1928);
    xsi_vlogvar_assign_value(t3, t4, 0, 0, 128);
    goto LAB2;

}

static void Always_36_1(char *t0)
{
    char t6[8];
    char t13[32];
    char t15[32];
    char t20[16];
    char t21[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t14;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t22;
    char *t23;
    char *t24;

LAB0:    t1 = (t0 + 3256U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 3592);
    *((int *)t2) = 1;
    t3 = (t0 + 3288);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(36, ng0);

LAB5:    xsi_set_current_line(37, ng0);
    xsi_set_current_line(37, ng0);
    t4 = ((char*)((ng2)));
    t5 = (t0 + 2088);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 32);

LAB6:    t2 = (t0 + 2088);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_signed_less(t6, 32, t4, 32, t5, 32);
    t7 = (t6 + 4);
    t8 = *((unsigned int *)t7);
    t9 = (~(t8));
    t10 = *((unsigned int *)t6);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB7;

LAB8:    xsi_set_current_line(44, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    xsi_vlog_get_part_select_value(t20, 64, t4, 127, 64);
    t5 = (t0 + 1608);
    xsi_vlogvar_assign_value(t5, t20, 0, 0, 1);
    goto LAB2;

LAB7:    xsi_set_current_line(37, ng0);

LAB9:    xsi_set_current_line(38, ng0);
    t14 = ((char*)((ng4)));
    t16 = (t0 + 1768);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    xsi_vlog_get_part_select_value(t15, 127, t18, 126, 0);
    xsi_vlogtype_concat(t13, 128, 128, 2U, t15, 127, t14, 1);
    t19 = (t0 + 1768);
    xsi_vlogvar_assign_value(t19, t13, 0, 0, 128);
    xsi_set_current_line(39, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    xsi_vlog_get_part_select_value(t20, 64, t4, 127, 64);
    t5 = (t0 + 1208U);
    t7 = *((char **)t5);
    xsi_vlog_unsigned_greatereq(t21, 64, t20, 64, t7, 64);
    t5 = (t21 + 4);
    t8 = *((unsigned int *)t5);
    t9 = (~(t8));
    t10 = *((unsigned int *)t21);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(42, ng0);
    t2 = (t0 + 1768);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1768);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 128);

LAB12:    xsi_set_current_line(37, ng0);
    t2 = (t0 + 2088);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng6)));
    memset(t6, 0, 8);
    xsi_vlog_signed_add(t6, 32, t4, 32, t5, 32);
    t7 = (t0 + 2088);
    xsi_vlogvar_assign_value(t7, t6, 0, 0, 32);
    goto LAB6;

LAB10:    xsi_set_current_line(40, ng0);
    t14 = (t0 + 1768);
    t16 = (t14 + 56U);
    t17 = *((char **)t16);
    t18 = (t0 + 1928);
    t19 = (t18 + 56U);
    t22 = *((char **)t19);
    xsi_vlog_unsigned_minus(t13, 128, t17, 128, t22, 128);
    t23 = ((char*)((ng5)));
    xsi_vlog_unsigned_add(t15, 128, t13, 128, t23, 128);
    t24 = (t0 + 1768);
    xsi_vlogvar_assign_value(t24, t15, 0, 0, 128);
    goto LAB12;

}


extern void work_m_00000000002521501394_1959729983_init()
{
	static char *pe[] = {(void *)Always_31_0,(void *)Always_36_1};
	xsi_register_didat("work_m_00000000002521501394_1959729983", "isim/testCircuit7_isim_beh.exe.sim/work/m_00000000002521501394_1959729983.didat");
	xsi_register_executes(pe);
}
