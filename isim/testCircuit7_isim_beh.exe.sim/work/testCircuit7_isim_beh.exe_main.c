/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000001175650423_1125536140_init();
    work_m_00000000002521501394_1959729983_init();
    work_m_00000000000271276870_0612205594_init();
    work_m_00000000003666989343_0136710641_init();
    work_m_00000000001338877083_4219749713_init();
    work_m_00000000001330888981_3823021424_init();
    work_m_00000000004061994634_2058991009_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000004061994634_2058991009");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
