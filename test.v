`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:48:08 09/08/2014 
// Design Name: 
// Module Name:    test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module test(a,b,c,d);
	input [`DATAWIDTH32-1:0]a,b;
	output [`DATAWIDTH32-1:0] c,d;
	
	

	
	Adder32 my_adder1 (.A(a),.B(b),.S(c));

   REG32 REG32_1 (.d(c),.q(d));
	

	
	endmodule
	



