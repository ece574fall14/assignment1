`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:55:05 09/01/2014 
// Design Name: 
// Module Name:    testCOMPEQ 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module testCOMPEQ;
	reg[`DATAWIDTH8 - 1 : 0] a8, b8;
	reg[`DATAWIDTH16 - 1 : 0] a16, b16;
	reg[`DATAWIDTH32 - 1 : 0] a32, b32;
	reg[`DATAWIDTH64 - 1 : 0] a64, b64;
	wire eq8, eq16, eq32, eq64;
	COMPEQ #(.DATA_WIDTH(`DATAWIDTH8))compeq8(.a(a8), .b(b8), .eq(eq8));
	COMPEQ #(.DATA_WIDTH(`DATAWIDTH16))compeq16(.a(a16), .b(b16), .eq(eq16));
	COMPEQ #(.DATA_WIDTH(`DATAWIDTH32))compeq32(.a(a32), .b(b32), .eq(eq32));
	COMPEQ #(.DATA_WIDTH(`DATAWIDTH64))compeq64(.a(a64), .b(b64), .eq(eq64));
	//integer i;
	initial begin
		a8 <= 8'b0;
		b8 <= 8'b0;
		a16 <= 16'b0;
		b16 <= 16'b0;
		a32 <= 32'b0;
		b32 <= 32'b0;
		a64 <= 64'b0;
		b64 <= 64'b0;
		#`DELAY
		a8 <= 8'b0100_1000;
		b8 <= 8'b1010_1100;
		a16 <= 16'b0100_1110_1010_0101;
		b16 <= 16'b0110_1011_1111_1001;
		a32 <= 32'b0100_1110_1010_0101_0110_1011_1111_1001;
		b32 <= 32'b0110_1011_1111_1001_0100_1110_1010_0101;
		a64 <= 64'b0100_1110_1010_0101_0110_1011_1111_1001_0110_1011_1111_1001_0100_1110_1010_0101;
		b64 <= 64'b0110_1011_1111_1001_0100_1110_1010_0101_0100_1110_1010_0101_0110_1011_1111_1001;
		#`DELAY
		a8 <= 8'b0;
		b8 <= 8'b0;
		a16 <= 16'b0;
		b16 <= 16'b0;
		a32 <= 32'b0;
		b32 <= 32'b0;
		a64 <= 64'b0;
		b64 <= 64'b0;
		#`DELAY
		a8 <= 8'b1100_1010;
		b8 <= 8'b1010_1001;
		a16 <= 16'b0111_1100_1000_1101;
		b16 <= 16'b0011_1000_1001_1010;
		a32 <= 32'b0011_1000_1001_1010_0011_1000_1001_1010;
		b32 <= 32'b1110_1001_1000_1001_0100_1010_1000_1101;
		a64 <= 64'b0100_1110_1010_0101_0011_1000_1001_1010_0011_1000_1001_1010_1111_1001_0100_0101;
		b64 <= 64'b1001_0100_1110_1010_0101_0100_1110_0101_0011_1000_1001_1010_0011_1000_1001_1010;
		#`DELAY
		a8 <= 8'b0;
		b8 <= 8'b0;
		a16 <= 16'b0;
		b16 <= 16'b0;
		a32 <= 32'b0;
		b32 <= 32'b0;
		a64 <= 64'b0;
		b64 <= 64'b0;
		#`DELAY
		a8 <= 8'b1111_1111;
		b8 <= 8'b1111_1111;
		a16 <= 16'b1111_1111_1111_1111;
		b16 <= 16'b1111_1111_1111_1111;
		a32 <= 32'b1111_1111_1111_1111_1111_1111_1111_1111;
		b32 <= 32'b1111_1111_1111_1111_1111_1111_1111_1111;
		a64 <= 64'b1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111;
		b64 <= 64'b1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111;
	end
	integer begin8, end8, delay8;
	always@(a8,b8)begin
		begin8 = $time;
		$display("8 bit begin: %d", begin8);
	end
	always@(eq8)begin
		end8 = $time;
		$display("8 bit end: %d", end8);
		delay8 = begin8 - end8;
		$display("8 bit: %d", delay8);
	end
	
	integer begin16, end16, delay16;
	always@(a16,b16)begin
		begin16 = $time;
	end
	always@(eq16)begin
		end16 = $time;
		delay16 = begin16 - end16;
		$display("16 bit: %d", delay16);
	end
	
	integer begin32, end32, delay32;
	always@(a32,b32)begin
		begin32 = $time;
	end
	always@(eq32)begin
		end32 = $time;
		delay32 = begin32 - end32;
		$display("32 bit: %d", delay32);
	end
	
	integer begin64, end64, delay64;
	always@(a64,b64)begin
		begin64 = $time;
	end
	always@(eq64)begin
		end64 = $time;
		delay64 = begin64 - end64;
		$display("64 bit: %d", delay64);
	end
endmodule
