`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:05:00 09/02/2014 
// Design Name: 
// Module Name:    testMUX2x1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module testMUX2x1;
	reg[`DATAWIDTH8 - 1 : 0] a8, b8;
	reg[`DATAWIDTH16 - 1 : 0] a16, b16;
	reg[`DATAWIDTH32 - 1 : 0] a32, b32;
	reg[`DATAWIDTH64 - 1 : 0] a64, b64;
	reg sel8, sel16, sel32, sel64;
	wire[`DATAWIDTH8 - 1 : 0] d8;
	wire[`DATAWIDTH16 - 1 : 0] d16;
	wire[`DATAWIDTH32 - 1 : 0] d32;
	wire[`DATAWIDTH64 - 1 : 0] d64;
	
	MUX2x1 #(.DATA_WIDTH(`DATAWIDTH8)) mux8(.a(a8), .b(b8), .d(d8), .sel(sel8));
	MUX2x1 #(.DATA_WIDTH(`DATAWIDTH16)) mux16(.a(a16), .b(b16), .d(d16), .sel(sel16));
	MUX2x1 #(.DATA_WIDTH(`DATAWIDTH32)) mux32(.a(a32), .b(b32), .d(d32), .sel(sel32));
	MUX2x1 #(.DATA_WIDTH(`DATAWIDTH64)) mux64(.a(a64), .b(b64), .d(d64), .sel(sel64));
	initial begin
		sel8 <= 1'b1;
		a8 <= 8'b0;
		b8 <= 8'b0;
		sel16 <= 1'b1;
		a16 <= 16'b0;
		b16 <= 16'b0;
		sel32 <= 1'b1;
		a32 <= 32'b0;
		b32 <= 32'b0;
		sel64 <= 1'b1;
		a64 <= 64'b0;
		b64 <= 64'b0;
		#`DELAY
		sel8 <= 1'b0;
		a8 <= 8'b0100_1000;
		b8 <= 8'b1010_1100;
		sel16 <= 1'b0;
		a16 <= 16'b0100_1110_1010_0101;
		b16 <= 16'b0110_1011_1111_1001;
		sel32 <= 1'b0;
		a32 <= 32'b0100_1110_1010_0101_0110_1011_1111_1001;
		b32 <= 32'b0110_1011_1111_1001_0100_1110_1010_0101;
		sel64 <= 1'b0;
		a64 <= 64'b0100_1110_1010_0101_0110_1011_1111_1001_0110_1011_1111_1001_0100_1110_1010_0101;
		b64 <= 64'b0110_1011_1111_1001_0100_1110_1010_0101_0100_1110_1010_0101_0110_1011_1111_1001;
		#`DELAY
		sel8 <= 1'b1;
		a8 <= 8'b1100_1010;
		b8 <= 8'b1010_1001;
		sel16 <= 1'b1;
		a16 <= 16'b0111_1100_1000_1101;
		b16 <= 16'b0011_1000_1001_1010;
		sel32 <= 1'b1;
		a32 <= 32'b0011_1000_1001_1010_0011_1000_1001_1010;
		b32 <= 32'b1110_1001_1000_1001_0100_1010_1000_1101;
		sel64 <= 1'b1;
		a64 <= 64'b0100_1110_1010_0101_0011_1000_1001_1010_0011_1000_1001_1010_1111_1001_0100_0101;
		b64 <= 64'b1001_0100_1110_1010_0101_0100_1110_0101_0011_1000_1001_1010_0011_1000_1001_1010;
		#`DELAY
		sel8 <= 1'b0;
		a8 <= 8'b1111_1111;
		b8 <= 8'b1111_1111;
		sel16 <= 1'b0;
		a16 <= 16'b1111_1111_1111_1111;
		b16 <= 16'b1111_1111_1111_1111;
		sel32 <= 1'b0;
		a32 <= 32'b1111_1111_1111_1111_1111_1111_1111_1111;
		b32 <= 32'b1111_1111_1111_1111_1111_1111_1111_1111;
		sel64 <= 1'b0;
		a64 <= 64'b1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111;
		b64 <= 64'b1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111;
	end
endmodule
