`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:25:04 09/04/2014 
// Design Name: 
// Module Name:    testSHL16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module testSHL16;
	reg[`DATAWIDTH16 - 1 : 0] a16;
	reg[$clog2(`DATAWIDTH16) - 1 : 0] sh16;
	wire[`DATAWIDTH16 - 1 : 0] d16;

	SHL #(.DATA_WIDTH(`DATAWIDTH16)) shl16(.a(a16), .sh_amt(sh16), .d(d16));
	initial begin
		a16 <= 16'b0100_1110_1010_0101;
		sh16 <= 10;
		#`DELAY
		a16 <= 16'b0111_1100_1000_1101;
		sh16 <= 10;
		#`DELAY
		a16 <= 16'b1111_1111_1111_1111;
		sh16 <= 10;
	end
endmodule
