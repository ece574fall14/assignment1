`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:53:30 09/04/2014 
// Design Name: 
// Module Name:    testSHL8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module testSHL8;
	reg[`DATAWIDTH8 - 1 : 0] a8;
	reg[$clog2(`DATAWIDTH8) - 1 : 0] sh8;
	wire[`DATAWIDTH8 - 1 : 0] d8;

	SHL #(.DATA_WIDTH(`DATAWIDTH8)) shl8(.a(a8), .sh_amt(sh8), .d(d8));
	initial begin
		a8 <= 8'b0100_1000;
		sh8 <= 5;
		#`DELAY
		a8 <= 8'b1100_1010;
		sh8 <= 5;
		#`DELAY
		a8 <= 8'b1111_1111;
		sh8 <= 5;
	end
endmodule
