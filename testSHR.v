`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:58:25 09/02/2014 
// Design Name: 
// Module Name:    testSHR 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module testSHR;
	reg[`DATAWIDTH8 - 1 : 0] a8;
	reg[`DATAWIDTH16 - 1 : 0] a16;
	reg[`DATAWIDTH32 - 1 : 0] a32;
	reg[`DATAWIDTH64 - 1 : 0] a64;
	reg[$clog2(`DATAWIDTH8) - 1 : 0] sh8;
	reg[$clog2(`DATAWIDTH16) - 1 : 0] sh16;
	reg[$clog2(`DATAWIDTH32) - 1 : 0] sh32;
	reg[$clog2(`DATAWIDTH64) - 1 : 0] sh64;
	wire[`DATAWIDTH8 - 1 : 0] d8;
	wire[`DATAWIDTH16 - 1 : 0] d16;
	wire[`DATAWIDTH32 - 1 : 0] d32;
	wire[`DATAWIDTH64 - 1 : 0] d64;

	SHR #(.DATA_WIDTH(`DATAWIDTH8)) shr8(.a(a8), .sh_amt(sh8), .d(d8));
	SHR #(.DATA_WIDTH(`DATAWIDTH16)) shr16(.a(a16), .sh_amt(sh16), .d(d16));
	SHR #(.DATA_WIDTH(`DATAWIDTH32)) shr32(.a(a32), .sh_amt(sh32), .d(d32));
	SHR #(.DATA_WIDTH(`DATAWIDTH64)) shr64(.a(a64), .sh_amt(sh64), .d(d64));
	initial begin
		a8 <= 8'b0100_1000;
		sh8 <= 5;
		a16 <= 16'b0100_1110_1010_0101;
		sh16 <= 10;
		a32 <= 32'b0100_1110_1010_0101_0110_1011_1111_1001;
		sh32 <= 20;
		a64 <= 64'b0100_1110_1010_0101_0110_1011_1111_1001_0110_1011_1111_1001_0100_1110_1010_0101;
		sh64 <= 40;
		#`DELAY
		a8 <= 8'b1100_1010;
		sh8 <= 5;
		a16 <= 16'b0111_1100_1000_1101;
		sh16 <= 10;
		a32 <= 32'b0011_1000_1001_1010_0011_1000_1001_1010;
		sh32 <= 20;
		a64 <= 64'b0100_1110_1010_0101_0011_1000_1001_1010_0011_1000_1001_1010_1111_1001_0100_0101;
		sh64 <= 40;
		#`DELAY
		a8 <= 8'b1111_1111;
		sh8 <= 5;
		a16 <= 16'b1111_1111_1111_1111;
		sh16 <= 10;
		a32 <= 32'b1111_1111_1111_1111_1111_1111_1111_1111;
		sh32 <= 20;
		a64 <= 64'b1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111_1111;
		sh64 <= 50;
	end
endmodule
