`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:56:44 09/08/2014 
// Design Name: 
// Module Name:    test_tb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "PARAMETERS.v"
module test_tb();
	
	reg [`DATAWIDTH32-1:0] a_s,b_s;
	wire [`DATAWIDTH32-1:0] c_s,d_s;

	
	test CompToTest (a_s,b_s,c_s,d_s);
	Adder32 my_adder1(A_s,B_s,S_s);
	REG32 REG32_1 (d_s,q_s,Clk_s,Rst_s);
	
	initial begin
	
	a_s<=23;
	b_s<=24;
	 
	#`DELAY
	
	a_s<=55;
	b_s<=66;
	
	#`DELAY

	a_s<=33; 
	b_s<=12;
	
	#`DELAY

	a_s<=64;
	b_s<=99;
	end
	

endmodule